module.exports = {
    multipleMongooseToObj: function (objArr) {
        return objArr.map(obj => obj.toObject());
    },
    mongooseToObj: function (obj) {
        return obj.toObject();
    },
};