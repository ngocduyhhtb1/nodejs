const Course = require('../model/Course');
const { mongooseToObj } = require('../../util/mongoose');

class CoursesController {
    info(req, res, next) {
        Course.findOne({ slug: req.params.slug })
            .then(courses => {
                res.render('courses/show', { courses: mongooseToObj(courses) });
            })
            .catch(next);
    }

    //[GET] /courses/create
    create(req, res, next) {
        res.render('courses/create');
    }

    //[POST] /courses/store
    store(req, res, next) {
        const formData = req.body;
        formData.img = `https://img.youtube.com/vi/${req.body.videoId}/sddefault.jpg`;
        const course = new Course(req.body);
        course.save()
            .then(() => res.redirect('/'))
            .catch(err => {

            })
    }
    delete(req, res, next) {
        const _id = req.body.id;
        Course.findByIdAndDelete(id)
            .then(course => { })
            .catch(err => {

            })
    }
}

module.exports = new CoursesController();
