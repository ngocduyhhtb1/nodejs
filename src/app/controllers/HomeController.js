const Course = require('../model/Course');
const {multipleMongooseToObj} = require('../../util/mongoose');

class HomeController {
    home(req, res, next) {
        Course.find({})
            .then(courses => {
                res.render('home', {
                    courses: multipleMongooseToObj(courses)
                })
            })
            .catch(next);
    }
}

module.exports = new HomeController();
