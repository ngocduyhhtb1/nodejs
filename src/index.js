const path = require('path');
const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const handlebars = require('express-handlebars');
const port = 8080;

//import routes
const route = require('./routes');
//import db
const db = require('./config/db');
//Connect MongoDB
db.connect();
// const route = require('Route');
app.use(express.static(path.join(__dirname, 'public')));
//Morgan
app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({extended: true}));

const hbs = handlebars.create({
    // Specify helpers which are only registered on this instance.
    helpers: {
        sum: function(a, b){
            return a + b;
        }
    }
});
//Set Engine
app.engine('hbs',
    handlebars({
        extname: 'hbs'
    })
);
app.engine('handlebars', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'resource', 'views'));

//Route
route(app);

app.listen(port, () => console.log(`App listening at http://localhost:${port}`));
