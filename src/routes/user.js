const express = require('express');
const route = express.Router();
const userController = require('../app/controllers/UserController');

route.get('/my-courses', userController.userCourses);
route.get('/', userController.userDashboard)

module.exports = route;