const homeRouter = require('./home');
const coursesRouter = require('./courses');
const userController = require('./user');

function route(app) {
    // app.use
    app.use('/user', userController);
    app.use('/courses', coursesRouter);
    app.use('/', homeRouter);
}

module.exports = route;
